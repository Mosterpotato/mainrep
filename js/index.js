/* Debemos activar el tooltip manualmente o no anda el tooltip, igual con el popover */
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });
    /*Este es el id del modal, al modal, cuando aparec que haga algo.*/
    $('#ingredientes').on('show.bs.modal', function (e) {
      console.log('Se muestra el Modal');
      $('#ingredientesBtnZapallo').removeClass('btn-outline-success');
      $('#ingredientesBtnZapallo').addClass('btn-primary');
      $('#ingredientesBtnZapallo').prop('disabled', true);
      $('#ingredientesBtnChivito').removeClass('btn-outline-success');
      $('#ingredientesBtnChivito').addClass('btn-primary');
      $('#ingredientesBtnChivito').prop('disabled', true);
      $('#ingredientesBtnPapaFrita').removeClass('btn-outline-success');
      $('#ingredientesBtnPapaFrita').addClass('btn-primary');
      $('#ingredientesBtnPapaFrita').prop('disabled', true);
      $('#ingredientesBtnTorta').removeClass('btn-outline-secondary');
      $('#ingredientesBtnTorta').addClass('btn-primary');
      $('#ingredientesBtnTorta').prop('disabled', true);

      var button = $(e.relatedTarget); // Button that triggered the modal
      var recipient = button.data('ingr'); // Extract info from data-* attributes

      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this);
      modal.find('.modal-title').text('Ingredientes de ' + recipient);


    });
    $('#ingredientes').on('shown.bs.modal', function (e) {
      console.log('Se muostro el Modal');
    });
    $('#ingredientes').on('hide.bs.modal', function (e) {
      console.log('Se escondera el Modal');
    });
    $('#ingredientes').on('hidden.bs.modal', function (e) {
      console.log('Se esconde el Modal');
      $('#ingredientesBtnZapallo').removeClass('btn-primary');
      $('#ingredientesBtnZapallo').addClass('btn-outline-success');
      $('#ingredientesBtnZapallo').prop('disabled', false);
      $('#ingredientesBtnChivito').removeClass('btn-primary');
      $('#ingredientesBtnChivito').addClass('btn-outline-success');
      $('#ingredientesBtnChivito').prop('disabled', false);
      $('#ingredientesBtnPapaFrita').removeClass('btn-primary');
      $('#ingredientesBtnPapaFrita').addClass('btn-outline-success');
      $('#ingredientesBtnPapaFrita').prop('disabled', false);
      $('#ingredientesBtnTorta').removeClass('btn-primary');
      $('#ingredientesBtnTorta').addClass('btn-outline-secondary');
      $('#ingredientesBtnTorta').prop('disabled', false);
    });
  });